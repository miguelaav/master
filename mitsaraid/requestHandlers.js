function iniciar(response, postData) {
  console.log("Manipulador de Peticion 'iniciar' fue llamado.");

  var body = '<html>'+
    '<head>'+
    '<script src="https://cdn.pubnub.com/pubnub-dev.js"></script>' +
    '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />'+
    '</head>'+
    '<body>'+
    '<form action="/resultado" method="post">'+
    '<textarea name="text" rows="20" cols="60"></textarea>'+
    '<input type="submit" value="Enviar texto" />'+
    '</form>'+
    '</body>'+
    '</html>';
  
    response.writeHead(200, {"Content-Type": "text/html"});
    response.write(body);
    response.end();
}

function resultado(response, dataPosteada) {
  console.log("Manipulador de Peticion 'resultado' fue llamado.");
     response.writeHead(200, {'Content-Type': 'text/html; charset=utf-8'});
        //response.writeHead(200, {"Content-Type": "text/html"});
  //response.write("Tu enviaste: " + dataPosteada);  
  var limpio=decodeURIComponent(dataPosteada).replace("∙%$&∙&%%/&&((/%&/%&/%&/%&/","").replace("&/($/%$&∙$&%$/&/(//&((/%&&%$/∙%∙&&%∙&","").replace("text=","").replace(/\+/g," ").replace(/\r?\n|\r/g," ");
  var limpio = "[" + limpio + "]";
  //response.write(limpio);
  var resultado = JSON.parse(limpio);
  response.write(resultado[0].title);
  response.write("<br>");
  response.write(resultado[0].text);
  //response.write(JSON.stringify(limpio))
  //response.json(dataPosteada);
  response.end();
}

exports.iniciar = iniciar;
exports.resultado = resultado;